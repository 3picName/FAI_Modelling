from sys import argv
import subprocess

filename = 'vars.txt'
infiles = 'ships.SAT'
outfiles = 'shipsout.SAT'

txt = open(filename)
content = txt.read()
slist = content.split('\n')
varlist = []

for i in range (0, len(slist)-1):
    if slist[i] == 'end':
        break
    else:
        varlist.append(slist[i])

with open(infiles) as infile, open(outfiles, 'w') as outfile:
    for line in infile:
        for src in varlist:
            line = line.replace(src, str(varlist.index(src)+1))
            line = line.replace('-'+src, str((-1)*(varlist.index(src)+1)))
            line = line.replace(';', ' 0')
        outfile.write(line)

p = subprocess.Popen('yices-sat -m -v '+outfiles, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
for line in p.stdout.readlines():
    print line,
retval = p.wait()
