(define 
(domain ex4)
(:requirements :typing :equality)
(:types cell robot number cluster)
(:constants: rob)
(:predicates
    (painted ?x - cell)                 ;; true iff this cell is painted
    (at ?x - cell)                      ;; true iff the robot is at this cell
    (adj ?x - cell ?y - cell)           ;; true iff x and y are adjecent cells
    (pred ?x - number ?y - number)      ;; true iff y = x-1 (predecessor)
    (val ?x - cluster ?y - number)      ;; true iff the value of this cluster is y
    (in_cluster ?x - cluster ?y - cell) ;; true iff cell y is in cluster x
    )

        
(:action paint
    ;; @param 
    ;;      c:      the cell to be painted, 
    ;;      c2:     an already painted adjacent cell, 
    ;;      c_rob:  the cell on which the robot is located, 
    ;;      id:     the id of the cluster which will be painted 
    ;;      v: the  number of missing cells in this cluser, 
    ;;      v_pred: the predecessor of v
    :parameters (?c - cell ?c2 - cell ?c_rob - cell ?id - cluster ?v - number ?v_pred - number)
    :precondition (and
        (pred ?v ?v_pred)
        (val ?id ?v)
        (not(painted ?c))
        (and (at ?c_rob) (adj ?c_rob ?c))
        (adj ?c ?c2) 
        (painted ?c2) 
        (in_cluster ?id ?c2)
        
        ;; there must not exist another cluster adjacent to the to be painted cell (otherwise clusters would merge)
        (not(exists(?c_bad - cell)(and
            (adj ?c ?c_bad)
            (painted ?c_bad)
            (not(in_cluster ?id ?c_bad)))))
    )

    :effect (and
        (painted ?c)            ;; paint the new cell
        (not(val ?id ?v))       ;; reassign cluster the new value
        (val ?id ?v_pred)       ;; which is the old one minus one, to say that a cell has been added. If it gets to 1 we are finished
        (in_cluster ?id ?c)     ;; assign the painted cell to the cluster
        )
)
    
(:action move
    :parameters (?from - cell ?to - cell)
    :precondition (and
        (at ?from)
        (adj ?from ?to)
        (not(painted ?to)))
    :effect (and
        (not(at ?from))
        (at ?to)))
        )
