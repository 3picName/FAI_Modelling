(define (problem problem1)
(:domain ex4)
(:objects 
    ;; declare all cells here
    c11 - cell c12 - cell c13 - cell c14 - cell
    c21 - cell c22 - cell c23 - cell c24 - cell
    c31 - cell c32 - cell c33 - cell c34 - cell
    c41 - cell c42 - cell c43 - cell c44 - cell
    
    ;; declare all numbers up to the one that is the size of the largest cluster here
    n0 - number n1 - number n2 - number n3 n4 - number
    
    ;; declare all clusters here
    id1 - cluster id2 - cluster id3 - cluster
)
(:init
    ;; initial position of the robot
    (at c11)
    
    ;; declare predecessor relationship between numbers
    (pred n4 n3) (pred n3 n2) (pred n2 n1) (pred n1 n0)
    
    ;; declare all adjecency relationships betwwen cells
    (adj c11 c12) (adj c11 c21)
    (adj c12 c11) (adj c12 c13) (adj c12 c21) (adj c12 c22) (adj c12 c23)
    (adj c13 c12) (adj c13 c23) (adj c13 c14)
    (adj c14 c13) (adj c14 c23) (adj c14 c24)
    (adj c21 c11) (adj c21 c12) (adj c21 c22) (adj c21 c31)
    (adj c22 c21) (adj c22 c12) (adj c22 c23) (adj c22 c33) (adj c22 c32) (adj c22 c31)
    (adj c23 c12) (adj c23 c13) (adj c23 c14) (adj c23 c24) (adj c23 c33) (adj c23 c22)
    (adj c24 c14) (adj c24 c23) (adj c24 c33) (adj c24 c34)
    (adj c31 c21) (adj c31 c22) (adj c31 c32) (adj c31 c41)
    (adj c32 c31) (adj c32 c22) (adj c32 c33) (adj c32 c43) (adj c32 c42) (adj c32 c41)
    (adj c33 c22) (adj c33 c23) (adj c33 c24) (adj c33 c34) (adj c33 c43) (adj c33 c32)
    (adj c34 c24) (adj c34 c33) (adj c34 c43) (adj c34 c44)
    (adj c41 c31) (adj c41 c32) (adj c41 c42)
    (adj c42 c41) (adj c42 c32) (adj c42 c43)
    (adj c43 c42) (adj c43 c32) (adj c43 c33) (adj c43 c34) (adj c43 c44)
    (adj c44 c43) (adj c44 c34)
    
    ;; declare cells that are initially painted
    (painted c21) (painted c14) (painted c41)
    
    ;; assign each cluster the number of cells it should consist of
    (val id1 n4) (val id2 n3) (val id3 n2)
    
    ;; assign the cells to the clusters
    (in_cluster id1 c21) (in_cluster id2 c14) (in_cluster id3 c41)
)
(:goal (and
    ;; final position of the robot
    (at c44)
    
    ;; the value of all your clusters has to be n1 (representing the number 1), which means that 
    ;; the cluster consists of the correct number of cells
    (val id1 n1)
    (val id2 n1)
    (val id3 n1)
)
)
)
